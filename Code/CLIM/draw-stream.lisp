(in-package :catfood)

(clim:define-presentation-type event-dag ())

(clim:define-presentation-method clim:present
    (event-stream (type event-dag) stream view &key)
  (clim:with-room-for-graphics (stream)
    (clim:format-graph-from-roots (list event-stream)
                                  (lambda (object stream)
                                    (clim:present object 'event-thing
                                                  :stream stream))
                                  #'successors
                                  :merge-duplicates t
                                  :stream stream)))

(clim:define-presentation-method clim:present
    (event (type event-thing) stream view &key)
  (princ event stream))

(defun print-graph (event &optional (depth 0))
  (fresh-line)
  (dotimes (n (* 4 depth))
    (write-char #\-))
  (prin1 event)
  (dolist (successor (successors event))
    (print-graph successor (1+ depth))))
