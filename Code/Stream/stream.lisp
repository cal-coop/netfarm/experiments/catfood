(in-package :catfood)

(defun program-from-local-file (pathname)
  (netfarm-slacker:compile-program-from-file
   (asdf:system-relative-pathname :catfood pathname)))

(defclass event-thing ()
  ((successors :computed t :reader successors))
  (:metaclass netfarm:netfarm-class))

(defclass event-stream (event-thing)
  ((wavefront :computed t :reader wavefront))
  (:scripts (program-from-local-file "Stream/event-stream.scm"))
  (:metaclass netfarm:netfarm-class))

(defmethod netfarm-client:object-requires-real-counters-p
    ((stream event-stream))
  "We don't need to observe negative counts."
  nil)

(defmethod initialize-instance :after ((stream event-stream) &key)
  (netfarm-scripts:run-script-machines stream))

(defclass event (event-thing)
  ((timestamp :initarg :timestamp
              :initform (get-universal-time)
              :reader timestamp)
   (last-wavefront :initarg :last-wavefront
                   :initform (alexandria:required-argument :last-wavefront)
                   :reader last-wavefront)
   (stream :initarg :stream :reader event-stream))
  (:scripts (program-from-local-file "Stream/event.scm"))
  (:metaclass netfarm:netfarm-class))

(defun make-event (event-stream
                   &rest initargs
                   &key (class (find-class 'event))
                        (run-script-machines? t))
  (let ((event
          (apply #'make-instance class
                 :last-wavefront (wavefront event-stream)
                 :stream event-stream
                 (alexandria:remove-from-plist initargs
                                               :class :run-script-machines?))))
    (when run-script-machines?
      (netfarm-scripts:run-script-machines event))
    event))

(defclass random-event (event)
  ((key :initform (random (expt 2 32)) :reader key))
  (:metaclass netfarm:netfarm-class))

(defun make-randomly-observed-events (event-stream
                                      &key (count 10))
  (let ((run-later '()))
    (dotimes (n count)
      (let* ((run?  (zerop (random 2)))
             (event (make-event event-stream
                                :class 'random-event
                                :run-script-machines? run?)))
        (unless run?
          (push event run-later))))
    (mapc #'netfarm-scripts:run-script-machines run-later)))
