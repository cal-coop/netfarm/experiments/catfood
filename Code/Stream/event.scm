(define (clear-wavefront wavefront stream)
  (if (null? wavefront)
      0
      (begin
        (call-method stream "remove-from-wavefront" (car wavefront))
        (call-method (car wavefront) "add-successor")
        (clear-wavefront (cdr wavefront) stream))))

(define-method (add-successor)
  (add-computed-value "successors" (sender)))

(define-method (initialize)
  ((lambda (event stream)
     (begin
       (clear-wavefront (object-value event "last-wavefront")
                        stream)
       (call-method stream "add-to-wavefront")))
   (self)
   (object-value (self) "stream")))
