(define-method (add-to-wavefront)
  (add-computed-value "wavefront" (sender)))

(define-method (remove-from-wavefront event)
  (remove-computed-value "wavefront" event))

(define-method (initialize)
  (call-method (self) "add-to-wavefront"))

(define-method (add-successor)
  (add-computed-value "successors" (sender)))
