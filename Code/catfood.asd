(asdf:defsystem :catfood
  :depends-on (:netfarm :netfarm-client :netfarm-slacker :clim)
  :components ((:file "package")
               (:module "Stream"
                :components ((:file "stream")))))
